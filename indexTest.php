<?php
require "bootstrap.php";
use Chatter\Models\Message; 
use Chatter\Models\User;
use Chatter\Middleware\Logging; 



$app = new \Slim\App();
$app->add(new Logging()); //add the midleware (Logging) layer

$app->get('/users/{id}', function($request, $response, $args){
    $name = User::find($args['id']);
    return $response->withStatus(200)->withJson(json_decode($name));
});


//READ USER - get the user information from database
$app->get('/users', function($request, $response,$args){
    //sleep(1);  
    $_user = new User(); //create new message
    $users = $_user->all();
    $payload = [];
    foreach($users as $usr){
        $payload[$usr->id] = [
            'name'=>$usr->name,
            'phone_number'=>$usr->phone_number,
            'city'=>$usr->city
        ];
    }
    return $response->withStatus(200)->withJson($payload);
});

$app->post('/users', function($request, $response,$args){
    $name = $request->getParsedBodyParam('name',''); //the user sheuzan
    $phone_number = $request->getParsedBodyParam('phone',''); //the user sheuzan
    $city = $request->getParsedBodyParam('city',''); //the user sheuzan
    $_user = new user(); //create new user
    $_user->name = $name;
    $_user->phone_number = $phone_number;
    $_user->city = $city;
    $_user->save();
    if($_user->id){
        $payload = ['user_id'=>$_user->id];
        return $response->withStatus(201)->withJson($payload);
    } else {
        return $response->withStatus(400);
    }
});
//DELETE USER - delete user from database
$app->delete('/users/{user_id}', function($request, $response,$args){
    $user = User::find($args['user_id']); //find the user in database
    $user->delete(); //delete the user
    if($user->exists){ //check if the user already exit
        return $response->withStatus(400); //exist -> false error
    } else {
        return $response->withStatus(200); //user delete succesfully
    }
});

//PUT USER - update user from database
$app->put('/users/{user_id}', function($request, $response,$args){
    $name = $request->getParsedBodyParam('name',''); //get the parameters from the post
    $phone = $request->getParsedBodyParam('phone',''); //get the parameters from the post
    $city = $request->getParsedBodyParam('city',''); //get the parameters from the post
    $_user = User::find($args['user_id']); // search the user with the id from the url
    $_user->name = $name; //delete the old param, and insert the new param
    $_user->phone_number = $phone;
    $_user->city = $city;
    if($_user->save()){ //the update succeed
        $payload = ['user_id'=>$_user->id, "result"=>"The user has been update succesfully"]; //useful to the json that return to the user about the update status
        return $response->withStatus(201)->withJson($payload); //succes
    } else{
        return $response->withStatus(400); //error
    }
});

//BULK USER - insert a number of users into the DB, with JSON input
$app->post('/users/bulk', function($request, $response,$args){
    $payload = $request->getParsedBody(); //convert the JSON to array of
    User::insert($payload); //insert to the DB all what come from the JSON
    return $response->withStatus(201)->withJson($payload);
});

$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);  
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

/*Ex 1
$app->get('/customers/{id}', function($request, $response,$args){
    $json = '{"1":"john", "2":"jack"}';
    $array = (json_decode($json, true));
    if(array_key_exists($args['id'], $array)){
        echo $array[$args['id']];
  }  
    else{
        echo "User not found.";
    }
});*/


    $app->run();



