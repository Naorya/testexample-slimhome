<?php
require "vendor/autoload.php";
$app = new \Slim\App();
$app->get('/customers/{id}/product/{number}', function($request, $response,$args){
   return $response->write('Customer '.$args['id'].' bought product number: '.$args['number']);
});
$app->run();
